import json
import pandas as pd
import boto3
import configparser
from io import BytesIO
from gzip import GzipFile


# Import config file
'''
config = configparser.ConfigParser()
config.read('config.ini')
'''
client = boto3.client('s3')
resource = boto3.resource('s3')
my_bucket = resource.Bucket('cloudflare-logpush')
good_columns = [
    "ClientRequestHost",
    "ClientRequestMethod",
    "ClientRequestPath",
    "ClientRequestURI",
    "WAFAction",
    "WAFFlags",
    "WAFMatchedVar",
    "WAFProfile",
    "WAFRuleID",
    "WAFRuleMessage"]

data = []


class wafdata():

    # Get list of all the files in the bucket, loops through each file and passes the file name to gets3()
    def getfiles(self):
        files = list(my_bucket.objects.filter(Prefix=''))
        length = len(files)
        for f in files:
            print(str(files.index(f)) + ' / ' + str(length))
            self.gets3(f.key)
        self.writedata()

    # Downloads file, unzips the gz file. Calls waf_parse() to parse the data
    def gets3(self, f):
        obj = client.get_object(Bucket=my_bucket.name, Key=f)
        bytestream = BytesIO(obj['Body'].read())
        got_text = GzipFile(None, 'rb', fileobj=bytestream).read().decode('utf-8')
        body = got_text.split('\n')
        self.waf_parse(body)

    # Parse the file and only append firewall events
    def waf_parse(self, body):
        for line in body:
            try:
                record = json.loads(line)
                if record['WAFAction'] != 'unknown':
                    selected_line = []
                    for item in good_columns:
                        selected_line.append(record[item])
                    data.append(selected_line)
            except:
                line
                continue
    
    # Write data to a csv
    def writedata(self):
        wafdata = pd.DataFrame(data, columns=good_columns)
        wafdata.to_csv('wafdata.csv', index=None, header=True)